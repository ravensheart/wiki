## Wiki setup

run these commands in a terminal
     
     git clone https://gitlab.com/little-monsters/ravensheart-wiki
     cd ravensheart-wiki
     npm install

## Starting the Wiki server

     ./server start

## using the wiki

Once th server is started go [http://localhost:3000](http://localhost:3000) in a browser.

## When your done

once your done run

    ./server stop

to kill the server.

