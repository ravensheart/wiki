created: 20180702195015649
modified: 20180704202757388
tags: [[Dice Game]]
title: Serpent's Dice
type: text/vnd.tiddlywiki

Serpent's Dice is a dice game played with five 12-sided dice. Each die is colored appropriate to one of the five elements, and the normal elemental dominance rules apply: Fire strengthens Water and Earth, Earth strengthens Metal and Wood, Metal strengthens Water and Fire, Water strengthens Wood and Earth, and Wood strengthens Fire and Metal. The inverse directions weaken; Water weakens Fire and Metal, etc. 

[img[5elements.png]]

The object of the game is to build two dragons containing a total of 5 dice. A dragon can be built with either two or three dice; one of the dice chosen are designated as "central", and the others subtract or add to it as per the elemental dominance. A dragon can only be made when the sum is 13. Once a dragon is built, the dice used to build the dragon are taken out of play and not rolled in future rolls (unless other things occur). In order to win the turn, the player must make his two dragons within 5 rolls. This can be extended by making a snake (any two dice that sum to 1), which resets the number of rolls, but is not kept (except the Oroboros).

However, whenever a pair is rolled on any set of active dice, something special occurs. This can frequently lead to a lost game. The other special case is the Oroboros. Whenever 5 of a kind are rolled, it is called the "Blessing of (animal)", and results in an instant win with double payout. Pairs are only considered on active dice; dice that have been used to construct a Dragon are not considered for this purpose.

* 6 and 7 Oroboros: This counts as both a Snake and a Dragon. The player may keep it as a dragon, and gets 5 rolls after finding the Oroboros. An Oroboros can only be constructed when the 7 strengthens the 6.<br>
* Pair of 12s Mauled by Tiger:  You lose the turn.<br>
* Pair of 11s Ox's Stubbornness: Gain 2 extra rolls past your current limit. <br>
* Pair of 10s Allicorn's Judgement: If you have no Dragons yet, you win the turn. If you have a Dragon, you lose the turn. <br>
* Pair of 9s Horse's Endurance: You can choose to not re-roll one of the 9s on your next roll. If you continue to roll 9s on other dice, you can continue to do this. <br>
* Pair of 8s Wild Boar's Recklessness: If there are any predators showing (12, 5, 3, 2, 1), you lose the turn. If you have a Dragon in play, ignore this result.<br>
* Pair of 7s Monkey Free Dragon: If you have a dragon, it is "freed", and you must roll all of your dice again. If you have lost any dice to Ferret's Mischief, they are restored.<br>
* Pair of 6s Crane's Grace: So long as there are no dice showing predators (12, 5, 3, 2, 1), you win the turn. Otherwise, nothing happens. Having a Dragon is considered to be having a predator.<br>
* Pair of 5s Dog's Loyalty:  You may exchange one or both of the dice that rolled 5s with the dice you have previously used to construct a Dragon, allowing you to change the elemental makup of your active dice.<br>
* Pair of 4s Rooster's Pride: Double your current bet, or forfeit the turn. <br>
* Pair of 3s Ferret's Mischief: One of your active dice is removed from play. If you are left with only one active die, you lose the turn. <br>
* Pair of 2s Cat's Lazyness: Lose two rolls. If you are left with no rolls, you lose the turn. <br>
* Pair of 1s Snake's Bite: You lose the turn. <br>

