created: 20180629154551406
modified: 20180629183127375
tags: [[Card Game]]
title: Attrition
type: text/vnd.tiddlywiki

<p>
Attrition is a common card game in Raven's Heart for 2-8 players (though some variants can have as many as 23 players in the early game). The standard, or elemental, variant is played with a 65 card deck. The deck has 5 suits, and 13 cards to a suit. The five suits correspond to the five elements, and the thirteen animals are the thirteen constallations. In the standard game, each suit has one trump suit: Wood trumps Earth, Earth trumps Water, Water trumps Fire, Fire trumps Metal, and Metal trumps Wood. 
</p>
<p>
Dealer shuffles and deals six cards to each player, starting with the player on the left (this can be important for some variants). The player to the dealer's left initiates a Challenge by playing a card from his hand. Play passes to the left, with each player responding to the challenge with a card in their hand. When responding to a challenge, the player must follow suit if they have any cards of that suit in their hand. If the do not, they can respond with a card from any other suit. All players must respond to every Challenge; it is not optional.
</p>
<p>
If no trumps were played, the player who played the highest in-suit card wins the Challenge. If any trumps were played, the highest trump wins instead. In the standard variant, only the trumps of the original suit are considered trumps; all other suits are trash, and always lose. 
</p>
<p>
The winner of the Challenge puts his winning card in his Bank and the rest of the cards from the Challenge into his hand. Anyone who has no cards left in their hand has been eliminated. The winner then initiates another Challenge, and play continues in this manner until all but one player has been eliminated. The winning player then gets one point for every card in his bank and in his hand, and deal passes to the right.
</p>
<p>
A number of minor variants of this game exist, from whether the cards in one's bank are kept face up or face down to who deals first to hand-size variants. The most common variant is a simple gambling variant where all players put some amount of money in the pot when the game begins, and the first player to reach a certain number of points (usually 100) wins the pot. Another basic variant has each point won in a game representing some amount of money from the pot, distributed when the deal ends. Play continues until all the money is won. Below are listed more major variations on the game.
</p>
<p>
<$list filter="[tag[High Magic Animals]]">

'' {{!!title}}''

<$transclude/>

</$list>

</p>

! Suit Variants
<$list filter="[tag[Attrition Suit Variant]]">

'' {{!!title}}''

<$transclude/>

</$list>

! Deal Variants

<$list filter="[tag[Attrition Deal Variant]]">

'' {{!!title}}''

<$transclude/>

</$list>

! Play Variants

<$list filter="[tag[Attrition Play Variant]]">

'' {{!!title}}''

<$transclude/>

</$list>

! Gambling Variants

<$list filter="[tag[Attrition Gambling Variant]]">

'' {{!!title}}''

<$transclude/>

</$list>