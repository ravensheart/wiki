created: 20180621144659583
modified: 20180621150519755
tags: core [[magic arts]] ethereal Core
title: High Magic
type: text/vnd.tiddlywiki


The magic of the Sky Elves, High Magic was created by the first Elf during the Working of Elf. It is never subtle nor quiet; as the High Mage speaks, symbols and colors flow around him as the world begins to shift to his will. High Magic is an Intellect Based Fatigue Powered Syntactic Core Magic. Use of High Magic was part of the reason why the Wood Elves and the Sky Elves split; Wood Elves, in general, see High Magic as dangerous.
High Magic is also symbolic; any character that trains in one trains in the other, for the words are the same. Unlike other forms of symbol magic, High Magic doesn't appear to be writing to the untrained eye. Rather, it appears as murals, paintings or intricately carved sculptures. Each working involves an element, a sacred animal and an aspect. The two aspects, Yin and Yang, define the flavor of the working and the color palate of the mural, while the element and animal define the content.
Syntactic High Magic needs an Aspect, an Element and an Animal. Each casting must use exactly one Aspect and one Element for each Animal invoked, and only those spells that transmute one thing to another must use more than one set. Multiple sets add to determine time, energy cost and skill penalty. 

All parameters are determined by threshold successes; see the Parameters document. 
Energy Recovery is a Yang-Wood-Dragon effect.

Aspects: 
Both aspects are Mental/Very Hard skills with a base cost of 0.

Yin: Passive, dark, female and cold, the yin aspect governs defensive magic, poison and magic that paralyzes or incapacitates. It's colors are what you would expect, with black, grey, blue, green, purple. Yin has a base time of 1.

Yang: Active, bright, male and hot, the yang aspect governs offensive magic, illusion, and magic that evokes strong emotions or makes people “burn” with passion. It's colors are vibrant and lively, with Red, Yellow, Orange, Brown and White being predominant. Yang has a base time of 0

Elements: Metal, Wood, Earth, Water, Fire
Metal: Solid, cold, discerning and dangerous, metal is a fundamental element to the dwarves while remaining unrecognized as such by most other races. It represents determination, dedication and mental strength, in addition to it's obvious physical meanings. Metal is Mental/Hard and has a base cost of 3 and a base time of 1.

Wood: Wood is life, in all of it's meanings. It is the roots of the trees that tear apart the mountain, the disease that ravages the old and a newborn baby. It may be held back temporarily, but never permanently stopped. It conquers all obstacles through determination and persistence. Wood heals, nurtures, and adapts. Wood is Mental/Average and has a base cost of 2 and a base time of 0.

Earth: Earth is everywhere, passive and resilient. You can never destroy the earth, and it is the source of all things. Earth deals with magic itself, and aids in meditative magic and rejuvenation of the mind and spirit, as well as divination. Earth is a Mental/Hard skill with a base cost of 3 and a base time of 1.

Water: Water is liquid, solid or steam, adaptable and flexible. It is almost as ubiquitous as earth, almost as persistent as wood and, as ice, almost as hard as metal. Water will even kill you as quickly as fire, under the right circumstances. Water governs emotion, motion and calmness. Water is Mental/Average with a base cost of 2 and a base time of 0.

Fire: Fire is destructive, emotional, consumptive, wild, hard to control and hot. It governs fiery emotions, destructiveness, light, illusion and other, related subjects. Fire is Mental/Average and has a base cost of 3. Fire has a special base time of -1, reducing the casting time of any spell it is part of. Overall casting time can still not go below 0.

Animals: 
All animal symbols are Mental/Average and have a base cost of 2.

Ferret: Forthright, tenacious, systematic, meticulous, charismatic, sensitive, hardworking, industrious, charming, eloquent, sociable, artistic, shrewd. Can be manipulative, vindictive, mendacious, venal, selfish, obstinate, critical, over-ambitious, ruthless, intolerant, scheming. Base casting time of 1.

Ox: Dependable, calm, methodical, born leader, patient, hardworking, ambitious, conventional, steady, modest, logical, resolute, tenacious. Can be stubborn, narrow-minded, materialistic, rigid, demanding. Base casting time of 3

Tiger: Unpredictable, rebellious, colorful, powerful, passionate, daring, impulsive, vigorous, stimulating, sincere, affectionate, humanitarian, generous. Can be restless, reckless, impatient, quick-tempered, obstinate, selfish, aggressive. Base casting time of 2

Rabbit/Cat: Luckiest of all, Gracious, good friend, kind, sensitive, soft-spoken, amiable, elegant, reserved, cautious, artistic, thorough, tender, self-assured, shy, astute, compassionate, flexible. Can be moody, detached, superficial, self-indulgent, opportunistic, stubborn. Base casting time of 1.

Dragon: Magnanimous, stately, vigorous, strong, self-assured, proud, noble, direct, dignified, zealous, eccentric, intellectual, fiery, passionate, decisive, pioneering, ambitious, artistic, generous, loyal. Can be tactless, arrogant, imperious, tyrannical, demanding, intolerant, dogmatic, violent, impetuous, brash. Base casting time of 2.

Snake: Deep thinker, wise, mystic, graceful, soft-spoken, sensual, creative, prudent, shrewd, ambitious, elegant, cautious, responsible, calm, strong, constant, purposeful. Can be loner, bad communicator, possessive, hedonistic, self-doubting, distrustful, mendacious, suffocating, cold. Base casting time of 1.

Horse: Cheerful, popular, quick-witted, changeable, earthy, perceptive, talkative, agile - mentally and physically, magnetic, intelligent, astute, flexible, open-minded. Can be fickle, arrogant, childish, anxious, rude, gullible, stubborn. Base casting time of 3.

Ram/Unicorn: Righteous, sincere, sympathetic, mild-mannered, shy, artistic, creative, gentle, compassionate, understanding, mothering, determined, peaceful, generous, seeks security. Can be moody, indecisive, over-passive, worrier, pessimistic, over-sensitive, complainer, weak-willed. Base casting time of 3.

Monkey: Inventor, motivator, improviser, quick-witted, inquisitive, flexible, innovative, problem solver, self-assured, sociable, artistic, polite, dignified, competitive, objective, factual, intellectual. Can be egotistical, vain, selfish, reckless, snodbbish, deceptive, manipulative, cunning, jealous, suspicious. Base casting time of 2, but can trade casting time for base cost, or vica versa, so long as neither go below 0.

Rooster: Acute, neat, meticulous, organized, self-assured, decisive, conservative, critical, perfectionist, alert, zealous, practical, scientific, responsible. Can be over zealous and critical, puritanical, egotistical, abrasive, opinionated, given to empty bravado. Base casting time of 3

Dog: Honest, intelligent, straightforward, loyal, sense of justice and fair play, attractive, amicable, unpretentious, sociable, open-minded, idealistic, moralistic, practical, affectionate, sensitive, easy going. Can be cynical, lazy, cold, judgmental, pessimistic, worrier, stubborn, quarrelsome. Base casting time of 3

Wild boar: Honest, gallant, sturdy, sociable, peace-loving, patient, loyal, hard-working, trusting, sincere, calm, understanding, thoughtful, scrupulous, passionate, intelligent. Can be naïve, over-reliant, self-indulgent, gullible, fatalistic, materialistic. Base casting time of 2.

Crane: Base Casting time 2



