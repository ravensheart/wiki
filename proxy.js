const express = require('express');
const request = require('request');
const app = express();

const port = 80;
const serverURL = 'http://localhost:8080';

const proxy = (req, res, next) => {
  console.log(`url: ${req.url}`);
  console.log(`method: ${req.method}`);
  req.pipe(request(serverURL + req.url)).pipe(res);
};

app.use(proxy);

app.listen(port, () => {
  console.log(`Raven's Heart Wiki started on port ${port}`);
});

